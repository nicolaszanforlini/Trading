
# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from datetime import datetime
import pandas as pd
from pandas import DataFrame

date = str(datetime.now())
date = date.split('.')[0]
date_f=date.split(' ')[0]

### class Data_Viz_mots_scores : dataviz des mots de l' entreprise choisi et de la date choisi ###

class Data_Viz_mots_scores:

    def __init__(self, dataframe):
        self.df = dataframe

    def bar_plot_mot_scorer(self):


        name = self.df['nom'][0]
        plt.clf()
        sns.set()
        sns_plot = sns.barplot(x=self.df['mot'], y=self.df['score'], data=self.df, color='springgreen')
        sns_plot.axes.set_title(name+' : Scoring news TF/IDF', fontsize=40,color='white')
        sns_plot.set_xlabel("mots", fontsize=25)
        sns_plot.set_ylabel("scores", fontsize=25)
        sns_plot.tick_params(labelsize=17)
        sns_plot.xaxis.label.set_color('white')
        sns_plot.tick_params(axis='x', colors='white')
        sns_plot.yaxis.label.set_color('white')
        sns_plot.tick_params(axis='y', colors='white')
        plt.xticks(rotation=10)
        plt.gcf().set_size_inches(15, 10)
        fig = sns_plot.get_figure()

        return fig

### class Data_Viz_variation : dataviz de la variation boursiere de l' entreprise ###

class Data_Viz_variation:

    def __init__(self, dataframe):

        self.df = dataframe

    def linear_plot_variation(self):

        plt.clf()
        sns_1 = sns.lineplot(x=self.df['date'], y=self.df['variation'], data=self.df, linewidth=3, marker='o', color='yellow')
        plt.xlabel("date", fontsize=25)
        sns_1.axes.set_title('Variation boursiere', fontsize=40,color='white')
        sns_1.set_ylabel("indice", fontsize=25)
        sns_1.tick_params(labelsize=17)
        sns_1.xaxis.label.set_color('white')
        sns_1.tick_params(axis='x', colors='white')
        sns_1.yaxis.label.set_color('white')
        sns_1.tick_params(axis='y', colors='white')
        plt.xticks(rotation=10)
        plt.gcf().set_size_inches(15, 10)
        fig_final = sns_1.get_figure()

        return fig_final

### class dataviz_nlp ###

class Data_Viz_nlp:

    def __init__(self, dataframe):
        self.df = dataframe

    def bar_plot_mot_scorer(self):

        name = 'NLP most_similarity_in_model'
        plt.clf()
        sns.set()
        sns_plot = sns.scatterplot(x=self.df['mots_proches'], y=self.df['angle'], data=self.df, color='red',s=200)
        sns_plot.axes.set_title(name, fontsize=40,color='white')
        sns_plot.set_xlabel("mots", fontsize=25)
        sns_plot.set_ylabel("scores", fontsize=25)
        sns_plot.tick_params(labelsize=17)
        sns_plot.xaxis.label.set_color('white')
        sns_plot.tick_params(axis='x', colors='white')
        sns_plot.yaxis.label.set_color('white')
        sns_plot.tick_params(axis='y', colors='white')
        plt.xticks(rotation=10)
        plt.gcf().set_size_inches(15, 10)
        fig_nlp = sns_plot.get_figure()

        return fig_nlp

### class Dataviz nlp etude mot tester : "perte" "profit" "hausse" "baisse" ###

class Dataviz_nlp_etude:

    def __init__(self, df):
        self.df = df

    def bar_plot_balance(self):
        name = 'NLP test_mots_in_model'
        plt.clf()
        sns.set()
        sns_plot = sns.barplot(x=self.df['score'], y=self.df['étude'], data=self.df, color='aqua')
        sns_plot.axes.set_title(name, fontsize=40, color='white')
        sns_plot.set_xlabel("score", fontsize=25)
        sns_plot.tick_params(labelsize=17)
        sns_plot.xaxis.label.set_color('white')
        sns_plot.tick_params(axis='x', colors='white')
        sns_plot.yaxis.label.set_color('white')
        sns_plot.tick_params(axis='y', colors='white')
        plt.xticks(rotation=10)
        plt.gcf().set_size_inches(15, 10)
        fig_nlp_balance = sns_plot.get_figure()

        return fig_nlp_balance






