
# -*- coding: utf-8 -*-
from gensim.models import Word2Vec
import pandas as pd
from pandas import DataFrame
from statistics import mean

modeleW2V='model/francais_corpus_1_et_2_2_sub_dim500_f'
model =Word2Vec.load(str(modeleW2V))

### class prediction pour faire une prediction (creer une dataframe pour une vizu) ###

class Analyse_nlp:

    def __init__(self, mot_i):
        self.mot = mot_i

    def analyse_nlp(self):
        liste_mots = []
        scores_mots = []
        mot = self.mot
        tmp_nlp = model.wv.most_similar_cosmul(self.mot)
        for elem in tmp_nlp:
            liste_mots.append(elem[0])
            scores_mots.append(round(elem[1], 2))
        df_nlp = pd.DataFrame({'mot': (mot), 'mots_proches': (liste_mots), 'angle': (scores_mots)})

        return df_nlp

### class nlp etude test des mot "profit" "perte" "hausse" "baisse" ###

class NLP_profit_perte_hausse_baisse:

    def __init__(self, liste):

        self.liste_liste_in = liste

    def test_profit(self):

        score_profit = []
        for elem in self.liste_liste_in:
            for el in elem:
                try:
                    score_profit.append(round(model.wv.distance(el, 'profit'), 2))
                except:
                    continue
        return round(mean(score_profit), 4)

    def test_perte(self):

        score_perte = []
        for elem in self.liste_liste_in:
            for el in elem:
                try:
                    score_perte.append(round(model.wv.distance(el, 'perte'), 2))
                except:
                    continue
        return round(mean(score_perte), 4)

    def test_hausse(self):

        score_hausse = []
        for elem in self.liste_liste_in:
            for el in elem:
                try:
                    score_hausse.append(round(model.wv.distance(el, 'hausse'), 2))
                except:
                    continue
        return round(mean(score_hausse), 4)

    def test_baisse(self):

        score_baisse = []
        for elem in self.liste_liste_in:
            for el in elem:
                try:
                    score_baisse.append(round(model.wv.distance(el, 'baisse'), 2))
                except:
                    continue
        return round(mean(score_baisse), 4)


