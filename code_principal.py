
# -*- coding: utf-8 -*-
from class_nettoyage_stopword_tfidf import *
from class_dataframe import *
from class_bdd import *
from datetime import datetime
from pymongo import MongoClient
from class_dataviz import *
from class_nlp import *
from class_documents_insert import *
from statistics import mean

date = str(datetime.now())
date = date.split('.')[0]
date_f=date.split(' ')[0]

### connection mongodb ###

client = MongoClient("mongodb://127.0.0.1:27017")
db = client.ia_evo

### collecte des données du cac40 ###

begin = Scrap('https://www.zonebourse.com/CAC-40-4941/composition/?col=4')
print('scrap des noms de sociétées du cac40...')
title = begin.title()
print('scrap des variations boursiere...')
indice = begin.indice()
print('scrap des actualitées...')
news = begin.news()
print('scrap terminé...')

### petit nettoyage des noms des entreprises ###

print('petit nettoyage des noms de société avec des replaces')
title_f =[]
for elem in title:
    title_f.append(elem.replace('..','').replace(' ','_').replace("'",'_'))

### nettoyage des news et sauvegarde en dataviz des mots scorer ###

for i,elem in enumerate(news):
    print('nettoyage de l_actualitée n°:{} avec des replaces et un regex'.format(i))
    net = Nettoyage(elem)
    nettoyage = net.nettoyage_lower_replace_caractere_regex()
    print('passage dans un stopword de l_actualitée nettoyée n°: {}'.format(i))
    stop = Stopword(nettoyage)
    stopword = stop.stopword()
    print('scorage par tfidf de l_actualitée n°: {}'.format(i))
    tfidf = Tfidf(stopword)
    try:
        mot, score = tfidf.tfidf_in()
    except:
        continue
    print('recuperation des 8 meilleurs mots de l_actualitée n°: {}'.format(i))
    recup = Recup_mot(mot,score,title_f)
    recup_mot,recup_score = recup.recup_mot()
    print('creation d_une dataframe pour stocker les mots récupérés pour l_actualitée n°: {}'.format(i))
    create = Dataframe_des_mots(date_f,title_f[i],recup_mot,recup_score)
    create_dataframe_mot_scorer = create.create_dataframe()

### NLP mots similaire du model ###

    print('analyse nlp : récupérer les 10 mots les plus proches du meilleur mot scorer dans le model')
    m = create_dataframe_mot_scorer.sort_values(by=['score'], ascending=False)['mot']
    for eleee in m:
        r = str(eleee)
        analyse = Analyse_nlp(r)
        try:
            analyse_best_mot = analyse.analyse_nlp()
            plot_nlp = Data_Viz_nlp(analyse_best_mot)
            bar_plot_nlp = plot_nlp.bar_plot_mot_scorer()
            print('sauvegarde des mots similaires proches sur un barplot pour une visualisation')
            bar_plot_nlp.savefig("static/nlp_most/{}.png".format(create_dataframe_mot_scorer['nom'][0]),transparent=True)
            break
        except:
            continue

    print('sauvegarde sur un barplot des 8 meilleurs mots scorers par tf/idf pour la news n°: {}'.format(i))
    for e in title_f:
        if create_dataframe_mot_scorer['nom'][0] == e:
            viz = Data_Viz_mots_scores(create_dataframe_mot_scorer)
            barplot_viz = viz.bar_plot_mot_scorer()
            barplot_viz.savefig("static/barplot/{}.png".format(e),transparent=True)
        else:
            continue

### creation d'un document pour l' insertion des données dans mongodb (actualitées) ###

test = Document_mongo(title_f)
test_v = test.doc_indice(indice,db)
test_n = test.doc_news(news,db)

### creation d_une dataframe à partir d_une requete mongo pour une dataviz de la variation boursiere ###

print('requete mongodb')
nomm =[]
tmmp =  db.variations_net.distinct('nom')
for elem in tmmp:
    if len(elem) < 3:
        continue
    else:
        nomm.append(elem)
date_final = []
variation_final = []
nom_final = []
for elem in nomm:
    tmmp =  db.variations_net.find({'nom':elem})
    for el in tmmp:
        date_final.append(el.get('date'))
        nom_final.append(el.get('nom'))
        print('##################################################')
        variation_final.append(el.get('variation'))
        print('creation d_une dataframe pour la variation')
        ins = Dataframe_variation_create(date_final,nom_final,variation_final)
        inst = ins.create_dataframe_variation()
        print('creation dataviz variation')
        inst_d = Data_Viz_variation(inst)
        inst_da = inst_d.linear_plot_variation()
        print('sauvegarde dataviz variation')
        inst_da.savefig("static/linear_plot/{}.png".format(elem), transparent=True)

print('model Word2Vec is loading...')

### requete mongodb pour effectuer une analyse par nlp et en afficher une dataviz ###

nomm =[]
tmmp =  db.variations_net.distinct('nom')
for elem in tmmp:
    nomm.append(elem)

for elem in nomm:
    if len(elem) < 3:
        continue
    else:
        print('recuperation de l_actu de {}'.format(elem))
        t = Requete_mongo(elem)
        tt = t.requete_mongo()
        print('test par nlp de chaques mots de l_actu avec les mots -perte-profit-hausse-baisse-')
        n = NLP_profit_perte_hausse_baisse(tt)
        profit = n.test_profit()
        perte = n.test_perte()
        hausse = n.test_hausse()
        baisse = n.test_baisse()
        pro_hau = []
        per_bai =[]
        print('moyenne de tous les scores des mots tester')
        pro_hau.append(profit)
        pro_hau.append(hausse)
        ppro_hhau = round(mean(pro_hau),4)
        per_bai.append(perte)
        per_bai.append(baisse)
        pper_bbai = round(mean(per_bai),4)
        score_final_nlp = []
        score_final_nlp.append(ppro_hhau)
        score_final_nlp.append(pper_bbai)
        print('creation dataframe pour une vizu')
        dff = Dataframe_nlp_profit_perte_hausse_baisse(score_final_nlp)
        dfff = dff.create_df_nlp()
        print('vizu')
        vizz = Dataviz_nlp_etude(dfff)
        viiz = vizz.bar_plot_balance()
        print('sauvegarde de la vizu pour {}'.format(elem))
        viiz.savefig("static/nlp_balance/{}.png".format(elem),transparent=True)
        print('##################################################')


