
# -*- coding: utf-8 -*-
import pandas as pd
from datetime import datetime
from pandas import DataFrame

date = str(datetime.now())
date = date.split('.')[0]
date_f=date.split(' ')[0]

### class dataframe create indice news : creer une dataframe des données recuperer ###

class Dataframe_variation_create:

    date_f = date_f

    def __init__(self, date, nom, variation):

        self.date = date
        self.nom = nom
        self.var = variation

    def create_dataframe_variation(self):

        df_tmp = pd.DataFrame({'date': (self.date), 'nom': (self.nom), 'variation': (self.var)})
        df_tmp['variation'] = df_tmp['variation'].astype('float')

        return df_tmp

### class create dataframe pour mot_scorer ###

class Dataframe_des_mots:

    def __init__(self,date,title,mot,score):

        self.date = date
        self.title = title
        self.mot = mot
        self.score = score

    def create_dataframe(self):

        df = pd.DataFrame({'date':self.date,'nom':self.title,'mot':self.mot,'score':self.score})

        return df

### class dataframe nlp etude test mots : "profit" "perte" "hausse" "baisse" ###

class Dataframe_nlp_profit_perte_hausse_baisse:

    def __init__(self, score):

        self.etude = ['profit-hausse', 'perte-baisse']
        self.score = score

    def create_df_nlp(self):
        df = pd.DataFrame({'score': self.score, 'étude': self.etude})

        return df
