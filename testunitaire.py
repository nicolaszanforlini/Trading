
# -*- coding: utf-8 -*-
from datetime import datetime
import unittest
import pandas as pd
from class_bdd import *
from class_dataframe import *
from class_nettoyage_stopword_tfidf import *
from class_dataviz import *
from class_nlp import *
from class_documents_insert import *
import numpy as np

date = str(datetime.now())
date = date.split('.')[0]
date_f=date.split(' ')[0]

### class all test : test mon code ###

class All_test(unittest.TestCase):

### test de la class creation dataframe news et indice ###

    def test_dataframe_variation(self):

        liste_7 = ['fjfjf']
        liste_3 = ['test']
        liste_5 = ['1.4']
        test_data = Dataframe_variation_create(liste_7,liste_3,liste_5)
        test_retour = test_data.create_dataframe_variation()
        self.assertIsInstance(test_data, Dataframe_variation_create)
        self.assertIsInstance(test_data.nom, list)
        self.assertIsInstance(test_data.var, list)
        self.assertIsInstance(test_data.date, list)
        self.assertIsInstance(test_retour,pd.DataFrame)

### test de la class creation dataframe mot et score ###

    def test_dataframe_mot(self):

        liste_3 = ['d','f']
        liste_5 = ['u','d']
        liste_6 = ['u', 'd']
        test_data = Dataframe_des_mots(date_f,liste_6,liste_5,liste_3)
        test_retour = test_data.create_dataframe()
        self.assertIsInstance(test_data, Dataframe_des_mots)
        self.assertIsInstance(test_data.title, list)
        self.assertIsInstance(test_data.mot, list)
        self.assertIsInstance(test_data.score, list)
        self.assertIsInstance(test_data.date, str)
        self.assertIsInstance(test_retour,pd.DataFrame)

### test de la class nettoyage ###

    def test_nettoyage(self):

        liste = ['f', 's']
        test_instance = Nettoyage(liste)
        test_retour = test_instance.nettoyage_lower_replace_caractere_regex()
        self.assertIsInstance(test_instance, Nettoyage)
        self.assertIsInstance(test_instance.liste_news, list)
        self.assertIsInstance(test_retour, list)

### test de la class stopword ###

    def test_stopword(self):

        liste= ['f', 's']
        test_instance = Stopword(liste)
        test_retour = test_instance.stopword()
        self.assertIsInstance(test_instance, Stopword)
        self.assertIsInstance(test_instance.liste_insert, list)
        self.assertIsInstance(test_retour, list)

### test de la class tfidf ###

    def test_tfidf(self):

        liste = ['f', 's']
        test_instance = Tfidf(liste)
        test_retour = test_instance.tfidf_in()
        self.assertIsInstance(test_instance, Tfidf)
        self.assertIsInstance(test_instance.liste_a_scorer, list)
        self.assertIsInstance(test_retour[0], list)
        self.assertIsInstance(test_retour[1], list)
        self.assertEqual(len(test_retour), 2)

### test de la class recup_mot ###

    def test_recup_mot(self):

        liste = ['f', 's']
        liste_1 = ['g','s']
        liste_2 = ['d','g']
        test_instance = Recup_mot(liste,liste_1,liste_2)
        test_retour = test_instance.recup_mot()
        self.assertIsInstance(test_instance, Recup_mot)
        self.assertIsInstance(test_instance.mot, list)
        self.assertIsInstance(test_instance.score, list)
        self.assertIsInstance(test_instance.title, list)
        self.assertIsInstance(test_retour[0], list)
        self.assertIsInstance(test_retour[1], list)
        self.assertEqual(len(test_retour), 2)

### test de la class scrap ###

    def test_scrap(self):

        string = 'url'
        test_url = Scrap(string)
        self.assertIsInstance(test_url, Scrap)
        self.assertIsInstance(test_url.url, str)

### test de la class dataviz mot scorer ###

    def test_barplot(self):

        liste_1 = ['d', 'f']
        liste_2 = [1.9, 2.3]
        liste_3 = ['test','test']
        df = pd.DataFrame({'nom':liste_3,'mot': liste_1, 'score': liste_2})
        test_instance = Data_Viz_mots_scores(df)
        self.assertIsInstance(test_instance, Data_Viz_mots_scores)
        self.assertIsInstance(test_instance.df, pd.DataFrame)

### test de la class dataviz indice boursier ###

    def test_linear(self):

        liste_2 = [1.9, 2.3]
        liste_3 = ['test','test']
        df = pd.DataFrame({'nom':liste_3, 'variation': liste_2})
        test_instance = Data_Viz_variation(df)
        self.assertIsInstance(test_instance, Data_Viz_variation)
        self.assertIsInstance(test_instance.df, pd.DataFrame)

### test de la class nlp ###

    def test_nlp(self):

        inst = Analyse_nlp('tester')
        self.assertIsInstance(inst, Analyse_nlp)
        test_inst = inst.analyse_nlp()
        self.assertIsInstance(test_inst, pd.DataFrame)
        self.assertIsInstance(inst.mot, str)

### test de la class dataviz_nlp ###

    def test_barplot_nlp(self):

        liste_1 = ['d', 'f']
        liste_2 = [1.9, 2.3]
        liste_3 = ['test','test']
        df = pd.DataFrame({'mot':liste_3,'mots_proches': liste_1, 'angle': liste_2})
        test_instance = Data_Viz_nlp(df)
        self.assertIsInstance(test_instance, Data_Viz_nlp)
        self.assertIsInstance(test_instance.df, pd.DataFrame)

### test de la class load mongo ###

    def test_load_mongo(self):

        dico = {'name':'nico'}
        inst = Load_mongo(dico)
        self.assertIsInstance(inst, Load_mongo)
        self.assertIsInstance(inst.dico, dict)

### test de la class documents mongo ###

    def test_document_mongo(self):

        client = MongoClient("mongodb://127.0.0.1:27017")
        db = client.ia_evo
        liste = ['f', 's']
        test_instance = Document_mongo(liste)
        self.assertIsInstance(test_instance, Document_mongo)
        self.assertIsInstance(test_instance.liste_n, list)
        self.assertEqual(db, client.ia_evo)

### test de la class Requete mongo ###

    def test_requete_mongo(self):

        inst = Requete_mongo('tester')
        self.assertIsInstance(inst, Requete_mongo)
        test_inst = inst.requete_mongo()
        self.assertIsInstance(test_inst, list)
        self.assertIsInstance(inst.name, str)

### test de la class Dataframe nlp etude profit perte hausse baisse ###

    def test_dataframe_nlp_etude_profit_perte(self):

        liste = [1.4534,0.4567]
        inst = Dataframe_nlp_profit_perte_hausse_baisse(liste)
        self.assertIsInstance(inst, Dataframe_nlp_profit_perte_hausse_baisse)
        test_inst = inst.create_df_nlp()
        self.assertIsInstance(test_inst, pd.DataFrame)
        self.assertIsInstance(inst.etude, list)
        self.assertIsInstance(inst.score,list)

### test de la class Dataviz nlp etude ###

    def test_barplot_nlp_etude_profit_perte(self):

        liste_1 = ['d', 'f']
        liste_2 = [1.9, 2.3]
        df = pd.DataFrame({'mots_proches': liste_1, 'angle': liste_2})
        test_instance = Dataviz_nlp_etude(df)
        self.assertIsInstance(test_instance, Dataviz_nlp_etude)
        self.assertIsInstance(test_instance.df, pd.DataFrame)

### test de la class NLP etude profit perte hausse baisse ###

    def test_nlp_etude_profit_perte_hausse_baisse(self):

        liste = [1.4567,5.4321,2.1234]
        inst = NLP_profit_perte_hausse_baisse(liste)
        self.assertIsInstance(inst, NLP_profit_perte_hausse_baisse)
        self.assertIsInstance(inst.liste_liste_in, list)


if __name__ ==  '__main__':
    unittest.main()



