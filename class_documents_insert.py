
# -*- coding: utf-8 -*-
from datetime import datetime
from pymongo import MongoClient
from class_nettoyage_stopword_tfidf import *

client = MongoClient("mongodb://127.0.0.1:27017")
db = client.ia_evo

date = str(datetime.now())
date = date.split('.')[0]
date_f=date.split(' ')[0]

class Document_mongo:

    def __init__(self,liste_nom):

        self.liste_n = liste_nom

    def doc_indice(self,liste_p,db):

        for i, elem in enumerate(self.liste_n):
            dic={}
            dic['date'] = date_f
            dic['nom'] = elem
            dic['variation'] = liste_p[i]
            dic_i = db.variations_net
            dic_i.insert_one(dic)

    def doc_news(self,liste_p,db):

        for i, elem in enumerate(self.liste_n):
            dic = {}
            nett = Nettoyage(liste_p[i])
            netto = nett.nettoyage_lower_replace_caractere_regex()
            sto = Stopword(netto)
            stopp = sto.stopword()
            dic['date'] = date_f
            dic['nom'] = elem
            dic['actu'] = stopp
            dic_i = db.news_net
            dic_i.insert_one(dic)


