
# -*- coding: utf-8 -*-
from flask import Flask, render_template, request
from pymongo import MongoClient
from datetime import datetime

client = MongoClient("mongodb://127.0.0.1:27017")
db = client.ia_evo


date = str(datetime.now())
date = date.split('.')[0]
date_f=date.split(' ')[0]

app = Flask(__name__)
app._static_folder = 'static'

@app.route("/")
def accueil():
    title_ff = []
    for elem in db.news_net.distinct('nom'):
        title_ff.append(elem)
    return render_template('index.html',nom_entreprises = title_ff)

@app.route("/analyse", methods=["GET"])
def analyse():
    entreprise = request.args.get('ent')
    lien_pred = '/static/nlp_most/{}/{}.png'.format(date_f,entreprise)
    lien_mots = '/static/barplot/{}/{}.png'.format(date_f,entreprise)
    lien_indice = '/static/linear_plot/{}/{}.png'.format(date_f,entreprise)
    lien_pred_balance = '/static/nlp_balance/{}/{}.png'.format(date_f,entreprise)
    return render_template('analyse.html', entreprise = entreprise, lien_mots = lien_mots, lien_indice = lien_indice,
                           lien_pred = lien_pred, lien_pred_balance = lien_pred_balance)


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)

    client.close()


